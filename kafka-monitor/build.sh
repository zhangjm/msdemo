set -ex

[ -z ${CS_TOOL} ] && { echo "CS_TOOL is not defined." ; exit 1; }
[ -z ${DOCKER_REGISTRY} ] && { echo "DOCKER_REGISTRY is not defined." ; exit 1; }
[ -z ${CS_KUB_MASTER} ] && { echo "CS_KUB_MASTER is not defined." ; exit 1; }
[ -z ${IMAGE_TAG} ] && { echo "IMAGE_TAG is not defined." ; exit 1; }
[ -z ${CS_KUB_NS} ] && { echo "CS_KUB_NS is not define." ; exit 1; }


echo "##########"
echo "BUILDING cloudsight/base and cloudsight/base-proto IMAGES"
echo "##########"

echo "DEBUG: git commit ${GIT_COMMIT}"

$CS_TOOL --registry ${DOCKER_REGISTRY} \
   --kubernetes-master ${CS_KUB_MASTER_DEV} \
   --kubernetes-ns ${CS_KUB_NS} \
   --tag ${IMAGE_TAG} --cs-dir . \
   build --images kafka-monitor
